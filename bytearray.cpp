#include "bytearray.h"
#include <string.h>
#include <sstream>
#include <iomanip>

namespace alrTools
{
    bytearray::bytearray() : std::vector<byte>() {}
    bytearray::bytearray(const bytearray &self) : std::vector<byte>(self) {}

    bytearray::bytearray(const char val) : std::vector<byte>(sizeof(char))
    {
        InitFromValue(val);
    }
    bytearray::bytearray(const unsigned char val) : std::vector<byte>(sizeof(unsigned char))
    {
        InitFromValue(val);
    }
    bytearray::bytearray(const short val) : std::vector<byte>(sizeof(short))
    {
        InitFromValue(val);
    }
    bytearray::bytearray(const unsigned short val) : std::vector<byte>(sizeof(unsigned short))
    {
        InitFromValue(val);
    }
    bytearray::bytearray(const int val) : std::vector<byte>(sizeof(int))
    {
        InitFromValue(val);
    }
    bytearray::bytearray(const unsigned int val) : std::vector<byte>(sizeof(unsigned int))
    {
        InitFromValue(val);
    }
    bytearray::bytearray(const long long val) : std::vector<byte>(sizeof(long long))
    {
        InitFromValue(val);
    }
    bytearray::bytearray(const unsigned long long val) : std::vector<byte>(sizeof(unsigned long long))
    {
        InitFromValue(val);
    }

    bytearray::bytearray(const unsigned char * ptr, size_t size) : std::vector<byte>(ptr, &ptr[size]) {}
    bytearray::bytearray(const char * str) : std::vector<byte>(str, str + std::strlen(str)) {}
    bytearray::bytearray(const std::string &str) : std::vector<byte>(str.begin(), str.end()) {}

    std::string bytearray::ToString() const
    {
        return std::string(this->begin(), this->end());
    }
    std::string bytearray::ToHexString() const
    {
        std::stringstream ss;
        ss << std::hex << std::uppercase;
        for (size_t i = 0; i < this->size(); i++)
        {
            ss << std::setw(2) << std::setfill('0') << static_cast<int>(this->at(i));
        }
        return ss.str();
    }
    short bytearray::ToShort(size_t offset) const
    {
        short ret = 0;
        if (offset < this->size())
            for (size_t i = 0; i < sizeof(short); i++)
            {
                ret |= static_cast<short>(this->at(offset) << (i * 8));
                if (++offset >= this->size())
                    break;
            }
        return ret;
    }
    int bytearray::ToInt(size_t offset) const
    {
        int ret = 0;
        if (offset < this->size())
            for (size_t i = 0; i < sizeof(int); i++)
            {
                ret |= static_cast<int>(this->at(offset) << (i * 8));
                if (++offset >= this->size())
                    break;
            }
        return ret;
    }
    long long bytearray::ToLongLong(size_t offset) const
    {
        long long ret = 0;
        if (offset < this->size())
            for (size_t i = 0; i < sizeof(long long); i++)
            {
                ret |= static_cast<long long>(this->at(offset) << (i * 8));
                if (++offset >= this->size())
                    break;
            }
        return ret;
    }
    bytearray& bytearray::operator=(const char &right)
    {
        return SetFromValue(right);
    }
    bytearray& bytearray::operator=(const unsigned char &right)
    {
        return SetFromValue(right);
    }
    bytearray& bytearray::operator=(const short &right)
    {
        return SetFromValue(right);
    }
    bytearray& bytearray::operator=(const unsigned short &right)
    {
        return SetFromValue(right);
    }
    bytearray& bytearray::operator=(const int &right)
    {
        return SetFromValue(right);
    }
    bytearray& bytearray::operator=(const unsigned int &right)
    {
        return SetFromValue(right);
    }
    bytearray& bytearray::operator=(const long long &right)
    {
        return SetFromValue(right);
    }
    bytearray& bytearray::operator=(const unsigned long long &right)
    {
        return SetFromValue(right);
    }
    bytearray& bytearray::operator=(const char * right)
    {
        this->assign(right, right + std::strlen(right));
        return *this;
    }
    bytearray& bytearray::operator=(const std::string &right)
    {
        this->assign(right.begin(), right.end());
        return *this;
    }
    void bytearray::append(const bytearray &data)
    {
        this->insert(this->end(), data.begin(), data.end());
    }
    bytearray bytearray::cut(size_t start_pos, size_t length) const
    {

        if (start_pos >= this->size())
            return bytearray();
        length = (length == -1) || (length > (this->size() - start_pos)) ? this->size() - start_pos : length;
        return bytearray(&this->at(start_pos), length);
    }
}