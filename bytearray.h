#ifndef BYTEARRAY_H
#define BYTEARRAY_H
#include <string>
#include <vector>

namespace alrTools
{
    typedef unsigned char byte;

    class bytearray : public std::vector<byte>
    {
    private:
        template<typename T> inline void InitFromValue(T val)
        {
            for (size_t i = 0; i < sizeof(T); i++)
                this->at(i) = (static_cast<byte>(val >> (i * 8)));
        }
        template<typename T> inline bytearray& SetFromValue(const T &val)
        {
            this->resize(sizeof(T));
            for (size_t i = 0; i < sizeof(T); i++)
                this->at(i) = (static_cast<byte>(val >> (i * 8)));
            return *this;
        }
    public: 
        bytearray();
        bytearray(const bytearray &self);

        bytearray(const char val);
        bytearray(const unsigned char val);
        bytearray(const short val);
        bytearray(const unsigned short val);
        bytearray(const int val);
        bytearray(const unsigned int val);
        bytearray(const long long val);
        bytearray(const unsigned long long val);

        bytearray(const unsigned char * ptr, size_t size);
        bytearray(const char * str);
        bytearray(const std::string &str);

        std::string ToString() const;
        std::string ToHexString() const;
        short ToShort(size_t offset = 0) const;
        int ToInt(size_t offset = 0) const;
        long long ToLongLong(size_t offset = 0) const;

        bytearray& operator=(const char &right);
        bytearray& operator=(const unsigned char &right);
        bytearray& operator=(const short &right);
        bytearray& operator=(const unsigned short &right);
        bytearray& operator=(const int &right);
        bytearray& operator=(const unsigned int &right);
        bytearray& operator=(const long long &right);
        bytearray& operator=(const unsigned long long &right);
        bytearray& operator=(const char * right);
        bytearray& operator=(const std::string &right);

        void append(const bytearray &data);
        bytearray cut(size_t start_pos, size_t length = -1) const;
    };
}
#endif // BYTEARRAY_H